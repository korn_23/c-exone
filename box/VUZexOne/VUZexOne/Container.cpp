#include"Container.h"

using namespace Space;


	Container::Container(int theLength, int theWidth, int theHeight, double theMaxW) {
		length = theLength;
		width = theWidth;
		height = theHeight;
		maxW = theMaxW;
	}

	Container::Container() {}

	Box & Container::operator[] (int index) {
		return container[index];
	}

	int Container::getContainerSize() {
		return container.size();
	}

	double Container::getTotalWeight() {
		double rez = 0.0;
		for (int i = 0; i < container.size(); i++) {
			rez += container[i].getWeight();
		}
		return rez;
	}

	int Container::getTotalValue() {
		int rez = 0;
		for (int i = 0; i < container.size(); i++) {
			rez += container[i].getValue();
		}
		return rez;
	}

	Box Container::getBox(int index) {
		return container[index];
	}

	int  Container::addBox(Box a) {
		double rez = 0.0;
		for (int i = 0; i < container.size(); i++) {
			rez += container[i].getWeight();
		}
		if (a.getWeight() + rez <= maxW) {
			container.push_back(a);
			return container.size();
		}
		else throw std::invalid_argument("incorrect weight");
	}

	void Container::deleteBox(int index) {
		container.erase(container.begin() + index);
	}



	namespace Space {
		std::ostream& operator<<(std::ostream& s, Container &c)
		{
			s << "�����:" << c.length << ", ";
			s << "������:" << c.width << ", ";
			s << "������:" << c.height << ", ";
			s << "������������ ��� ����������� :" << c.maxW;
			s << "\n\n���������� ���������� : ";
			for (int i = 0; i < c.container.size(); i++) {
				s << "\n{" << c.container[i] << " }";
			}

			return s;
		}

		std::istream& operator>> (std::istream& is, Container& c)
		{
			is >> c.length;
			is >> c.width;
			is >> c.height;
			is >> c.maxW;
			return is;
		}
	}
