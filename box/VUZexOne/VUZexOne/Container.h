#pragma once
#include<vector>
#include "Box.h"

namespace Space {

	class Container {
	private:
		std::vector<Box> container;
		int length;
		int width;
		int height;
		double maxW;
	public:
		Container(int theLength, int theWidth, int theHeight, double theMaxW);

		Container();

		Box & operator[] (int index);
	

		int getContainerSize();

		double getTotalWeight();

		int getTotalValue();

		Box getBox(int index);

		int  addBox(Box a);

		void deleteBox(int index);
		
		friend std::istream& operator>> (std::istream& is, Container& c);
		friend std::ostream& operator<<(std::ostream& s, Container &c);

	};


	std::ostream& operator<<(std::ostream& s, Container &c);

	std::istream& operator>> (std::istream& is, Container& c);
}

